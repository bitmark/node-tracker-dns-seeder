#include "log.h"

// Function to get the current timestamp as a string
char* get_timestamp() {
    time_t now = time(NULL);
    return ctime(&now);  // Returns a string representing the current time
} // replace by ...
// Function to get the current time as Unix epoch seconds
time_t get_unix_time() {
     return time(NULL);  // Returns the current time as Unix epoch seconds
}

// Function to get the current time as a high-resolution timestamp std::string
std::string sGet_hi_res_timestamp() {
    auto now = std::chrono::system_clock::now();  // Get the current point in time
    auto now_c = std::chrono::system_clock::to_time_t(now);  // Convert to time_t
    auto now_ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;  // Get milliseconds part

    std::tm now_tm = *std::localtime(&now_c);
    char buffer[100];
    std::strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &now_tm);
    sprintf(buffer, "%s.%03ld", buffer, now_ms.count());  // Append milliseconds to the timestamp
    return std::string(buffer);
}

// Function to get the current time as a high-resolution timestamp char *
char* cGet_hi_res_timestamp() {
    static char buffer[100];
    auto now = std::chrono::system_clock::now();
    auto now_c = std::chrono::system_clock::to_time_t(now);
    auto now_ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;

    std::tm now_tm = *std::localtime(&now_c);
    std::strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &now_tm);
    sprintf(buffer, "%s.%03ld", buffer, now_ms.count()); // Append milliseconds to the timestamp
    return buffer;
}

// Function to log a message
void ut_log_message(FILE *fp, const char *message) {
    if (fp != NULL) {
        fprintf(fp, "%ld: %s\n", get_unix_time(), message);
        fflush(fp);  // Flush the output buffer to ensure it is written to the file
    }
}

// Function to log a message with high-resolution timestamp using  FILE*
void hr_log_message(FILE* fp, const char* message) {
    if (fp != NULL) {
        fprintf(fp, "[%s] %s\n", cGet_hi_res_timestamp(), message);
        fflush(fp);  // Ensure the message is written immediately
    }
}

// Function to log a message
void ut_log(const char *message) {
    if (log_file != NULL) {
      fprintf(log_file, "%ld: %s\n", get_unix_time(), message);
      fflush(log_file);  // Flush the output buffer to ensure it is written to the file
    }
}

// Function to log a message with high-resolution timestamp using  FILE*
void hr_log(const char* message) {
    if (log_file != NULL) {
        fprintf(log_file, "[%s] %s\n", cGet_hi_res_timestamp(), message);
        fflush(log_file);  // Ensure the message is written immediately
    }
}
