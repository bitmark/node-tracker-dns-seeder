CXXFLAGS = -O3 -g

dnsseed: dns.o bitcoin.o netbase.o protocol.o db.o main.o util.o log.o
	g++ -pthread $(LDFLAGS) -o dnsseed dns.o bitcoin.o netbase.o protocol.o db.o main.o util.o log.o -lcrypto -lcurl -lconfig++ -lncurses

%.o: %.cpp *.h
	g++ -std=c++11 -pthread $(CXXFLAGS) -Wall -Wno-unused -Wno-sign-compare -Wno-reorder -Wno-comment -c -o $@ $<

clean:
	rm -f *.o dnsseed

cleandata:
	rm -f dnsstats.log dnsseed.log dnsseed.dump dnsseed.dat

cleanall: clean cleandata
