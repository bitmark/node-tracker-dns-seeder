#pragma once
#ifndef DNSSEEDER_LOG_H
#define DNSSEEDER_LOG_H

#include <time.h> 
#include <cstdio>
#include <chrono>
#include <ctime>
#include <string>

extern FILE* log_file;

// Function to log a message with unix epoch secons
void ut_log_message(FILE *fp, const char *message);

// Function to log a message with high-resolution timestamp using  FILE*
void hr_log_message(FILE* fp, const char* message);

// Function to log a message
void ut_log(const char *message);

// Function to log a message with high-resolution timestamp using  FILE*
void hr_log(const char* message);

#define MAX_LOG 2048

#define logprintf(msg, args...) \
  { \
    char *msg_buffer = (char*)malloc(MAX_LOG+1);	\
    snprintf(msg_buffer, MAX_LOG, msg, ##args);		\
    ut_log(msg_buffer);	\
    free(msg_buffer);			\
  }

#endif
